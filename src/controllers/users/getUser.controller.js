const escape = require("escape-html");
const QueryDatabase = require("../../utils/queryDatabase");
const logger = require("../../loggers/loggers.config");

const GetUser = async (req, res, next) => {
  try {
    const sql = `
      SELECT * FROM "user";
    `;

    const data = await QueryDatabase(sql);
    res.status(200).send(
      data.rows.map((row) => {
        delete row.password;
        return row;
      }),
    );
  } catch (error) {
    logger.error(error);
    res.status(500).json({code: 500, message: "Internal Server Error"});
  }
};

const GetUserById = async (req, res, next) => {
  try {
    const id = escape(req.params.id);
    const sql = `
      SELECT * FROM "user" WHERE id = '${id}'
    `;

    const data = await QueryDatabase(sql);
    res.status(200).send(
      data.rows.map((row) => {
        delete row.password;
        return row;
      }),
    );
  } catch (error) {
    logger.error(error);
    res.status(500).json({code: 500, message: "Internal Server Error"});
  }
};

module.exports = {
  GetUser,
  GetUserById,
};
